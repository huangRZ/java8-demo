package com.itcast.java8.lambda;

import com.itcast.java8.bean.Employee;
import com.itcast.java8.interfaces.CompareInterface;
import com.itcast.java8.interfaces.Function;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * create by IntelliJ IDEA
 * User: HuangRZ
 * Email: huangrz11@163.com
 * Date: 2018/2/22 0022
 * Time: 14:23
 * version: 1.0
 * Description: 尚硅谷 - java8新特性 - lambda练习
 **/
public class LambdaTest {

    private List<Employee> employees;

    @Before
    public void before(){
        employees = Arrays.asList(
                new Employee("陈二", 34),
                new Employee("张三", 43),
                new Employee("李四", 55),
                new Employee("王五", 26),
                new Employee("赵六", 23)
        );
    }

    @Test
    public void sortTest(){
        employees.sort((a, b) -> {
            if (a.getAge().equals(b.getAge())) {
                return a.getName().compareTo(b.getName());
            } else {
                return a.getAge().compareTo(b.getAge());
            }
        });

        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }

    @Test
    public void toUpperCaseTest(){
        String s1 = toUpperCase("shjiheinf", str -> str.toUpperCase());
        String s2 = toUpperCase("\t\t\t  shjiheinf    ", str -> str.trim());
        System.out.println(s1);
        System.out.println(s2);
    }

    private String toUpperCase(String str, CompareInterface com){
        return com.getValue(str);
    }

    @Test
    public void calcTest(){
        System.out.println(calc(12L, 32L, (a, b) -> a+b));
        System.out.println(calc(13L, 20L, (a, b) -> a*b));
    }
    private Long calc(Long l1, Long l2, Function<Long, Long> lt){
        return lt.getValue(l1, l2);
    }

}
