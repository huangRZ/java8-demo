package com.itcast.java8.lambda;

import com.itcast.java8.interfaces.Function;
import org.junit.Test;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * create by IntelliJ IDEA
 * User: HuangRZ
 * QQ: 917647409
 * Email: huangrz11@163.com
 * Date: 2018/2/22 0022
 * Time: 19:18
 * version: 1.0
 * Description: java8 - 新特性 - 方法引用和构造器引用
 *
 * 一、方法引用：若 Lambda 体中的功能，已经有方法提供了实现，可以使用方法引用
 *               （可以将方法引用理解为 Lambda 表达式的另外一种表现形式）
 *
 * 1. 对象的引用 :: 实例方法名
 *
 * 2. 类名 :: 静态方法名
 *
 * 3. 类名 :: 实例方法名
 *
 * 注意：
 *      ①方法引用所引用的方法的参数列表与返回值类型，需要与函数式接口中抽象方法的参数列表和返回值类型保持一致！
 *      ②若Lambda 的参数列表的第一个参数，是实例方法的调用者，第二个参数(或无参)是实例方法的参数时，格式： ClassName::MethodName
 *
 * 二、构造器引用 :构造器的参数列表，需要与函数式接口中参数列表保持一致！
 *
 * 1. 类名 :: new
 *
 * 三、数组引用
 *
 *     类型[] :: new;
 *
 *
 **/
public class MethodRefTest {

    //数组引用
    @Test
    public void test(){
//        Function<Integer, String[]> fun = (args) -> new String[args];
    }

}
