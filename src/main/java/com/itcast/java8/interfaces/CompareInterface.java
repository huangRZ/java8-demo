package com.itcast.java8.interfaces;

/**
 * create by IntelliJ IDEA
 * User: HuangRZ
 * QQ: 917647409
 * Email: huangrz11@163.com
 * Date: 2018/2/22 0022
 * Time: 14:49
 * version: 1.0
 * Description:
 **/
@FunctionalInterface
public interface CompareInterface {

    String getValue(String str);

}
